/*
 * uart.h
 *
 *  Created on: Oct 23, 2018
 *      Author: nxp89240
 */

#ifndef UART_H_
#define UART_H_

#include "board.h"

#define EXAMPLE_USART USART0
#define EXAMPLE_USART_CLK_SRC kCLOCK_MainClk
#define EXAMPLE_USART_CLK_FREQ CLOCK_GetFreq(EXAMPLE_USART_CLK_SRC)
#define EXAMPLE_USART_IRQn USART0_IRQn

/*! @brief Buffer size (Unit: Byte). */
#define RX_BUFFER_SIZE 8

void EXAMPLE_USARTInit(void);
void EXAMPLE_USARTSendToTerminal(uint8_t data[],uint8_t length);

#endif /* UART_H_ */
