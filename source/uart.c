/*
 * uart.c
 *
 *  Created on: Oct 23, 2018
 *      Author: nxp89240
 */
#include "uart.h"
#include "fsl_usart.h"

/*******************************************************************************
 * Variables
 ******************************************************************************/
uint8_t rxBuffer[RX_BUFFER_SIZE];
volatile uint8_t rxDataCounter = 0U;
volatile bool rxBlockReceived = false;
volatile bool txBlockSent = false;


void USART0_IRQHandler(void)
{
    uint8_t data;

    /* If this Tx read flag is set, send data to buffer. */
    if (kUSART_TxReady & USART_GetStatusFlags(EXAMPLE_USART))
    {
    	txBlockSent= true;
    }

    /* If this Rx read flag is set, read data to buffer. */
    if (kUSART_RxReady & USART_GetStatusFlags(EXAMPLE_USART))
    {
        data = USART_ReadByte(EXAMPLE_USART);
        rxBuffer[rxDataCounter++] = data;
        if (rxDataCounter == RX_BUFFER_SIZE)
        {
        	rxDataCounter =0;
            /* Set the rxBlockreceived to true, and start a new line. */
        	rxBlockReceived = true;
        }

    }
}



void EXAMPLE_USARTInit(void)
{
    usart_config_t config;
    /* Default config by using USART_GetDefaultConfig():
     * config.baudRate_Bps = 9600U;
     * config.parityMode = kUSART_ParityDisabled;
     * config.stopBitCount = kUSART_OneStopBit;
     * config.bitCountPerChar = kUSART_8BitsPerChar;
     * config.loopback = false;
     * config.enableRx = false;
     * config.enableTx = false;
     * config.syncMode = kUSART_SyncModeDisabled;
     */
    USART_GetDefaultConfig(&config);
    config.enableRx = true;
    config.enableTx = true;
    config.baudRate_Bps = BOARD_DEBUG_USART_BAUDRATE;

    /* Initialize the USART with configuration. */
    USART_Init(EXAMPLE_USART, &config, EXAMPLE_USART_CLK_FREQ);


    /* Enable USART RX ready interrupt. */
    USART_EnableInterrupts(EXAMPLE_USART, kUSART_RxReadyInterruptEnable);
    EnableIRQ(EXAMPLE_USART_IRQn);
}

void EXAMPLE_USARTSendToTerminal(uint8_t data[], uint8_t length)
{
    /* Send log info to terminal in polling-mode. */
    USART_WriteBlocking(EXAMPLE_USART, data, length);

    /* Send back the received characters to terminal in interrupt-mode. */
    USART_EnableInterrupts(EXAMPLE_USART, kUSART_TxReadyInterruptEnable);

    /* Waitting for the Tx complete. */
    while (!txBlockSent)
    {
    }
    /* Reset the txDataCounter, rxDataCounter and rxNewLineDetected for next loop. */
    txBlockSent = 0U;

}
