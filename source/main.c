/*
 * The Clear BSD License
 * Copyright (c) 2017, NXP Semiconductors, Inc.
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided
 *  that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS LICENSE.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include "string.h"
#include "pin_mux.h"
#include "uart.h"
#include "board.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/

#define BOARD_PORT 0U

#define BOARD_PIN1 13U
#define BOARD_PIN2 14U
#define BOARD_LED2 16U

#define P1_HIGH		      GPIO_PortSet(GPIO, BOARD_PORT, 1u << BOARD_PIN1)
#define P1_LOW		      GPIO_PortClear(GPIO, BOARD_PORT, 1u << BOARD_PIN1)
#define P1_TOGGLE	      GPIO_PortToggle(GPIO, BOARD_PORT, 1u << BOARD_PIN1)

#define P2_HIGH		      GPIO_PortSet(GPIO, BOARD_PORT, 1u << BOARD_PIN2)
#define P2_LOW		      GPIO_PortClear(GPIO, BOARD_PORT, 1u << BOARD_PIN2)
#define P2_TOGGLE		  GPIO_PortToggle(GPIO, BOARD_PORT, 1u << BOARD_PIN2)

#define LED_OFF		      GPIO_PortSet(GPIO, BOARD_PORT, 1u << BOARD_LED2)
#define LED_ON	      GPIO_PortClear(GPIO, BOARD_PORT, 1u << BOARD_LED2)

#define IOCON_DPP_PD (0<<14 | 0<<10 | 1<<7 | 0<<6 | 1<<5 | 1<<3)

/*******************************************************************************
 * Prototypes
 ******************************************************************************/


/*******************************************************************************
 * Variables
 ******************************************************************************/

/*
* RxBuffer[0 - 3] - ON Time
* RxBuffer[4 - 7] - OFF Time
* RxBuffer[8 - 11] - Frequency
* RxBuffer[12 - 15] - Duty Cycle
*/
union{
	struct {

	volatile uint32_t OnTime:16;
	volatile uint32_t OffTime:16;
	volatile uint32_t Count:16;
	volatile uint32_t Delay:16;
	};
	volatile uint8_t Data[RX_BUFFER_SIZE];
}port;

extern volatile uint8_t rxBlockReceived;
extern uint8_t rxBuffer[RX_BUFFER_SIZE];

volatile uint8_t p1_off_flag=0;
volatile uint8_t p1_on_flag=0;
volatile uint8_t p1_start_flag=0;
volatile uint8_t p1_end_delay_flag=0;
volatile uint8_t start_task=0;

volatile uint32_t systickCounter;

/*******************************************************************************
 * Code
 ******************************************************************************/
void SysTick_Handler(void)
{
      systickCounter++;
}

/*!
 * @brief Main function
 */
int main(void)
{
    /* Define the init structure for the output LED pin*/
    gpio_pin_config_t pin1_config_input = {
        kGPIO_DigitalInput, 1,
    };

    gpio_pin_config_t pin1_config_output = {
        kGPIO_DigitalOutput, 1,
    };

    gpio_pin_config_t pin2_config_input = {
        kGPIO_DigitalInput, 0,
    };

    gpio_pin_config_t pin2_config_output = {
        kGPIO_DigitalOutput, 0,
    };
    /* Enable clock of uart0. */
    CLOCK_EnableClock(kCLOCK_Uart0);
    /* Ser DIV of uart0. */
    CLOCK_SetClkDivider(kCLOCK_DivUsartClk,1U);    

    BOARD_InitPins();
    BOARD_BootClockIRC12M();

    /* Init GPIO. */
    GPIO_PinInit(GPIO, BOARD_PORT, BOARD_PIN1, &pin1_config_input);
    GPIO_PinInit(GPIO, BOARD_PORT, BOARD_PIN2, &pin2_config_output);
    GPIO_PinInit(GPIO, BOARD_PORT, BOARD_LED2, &pin1_config_output);
    /* Set systick reload value to generate 10us interrupt */
    if(SysTick_Config(SystemCoreClock / 100000U))
    {
        while(1)
        {
        }
    }
    /* Initialize the USART instance with configuration, and enable receive ready interrupt. */
    EXAMPLE_USARTInit();

    while (1)
    {

  	  if(rxBlockReceived){

  		  memcpy(port.Data, rxBuffer, RX_BUFFER_SIZE);
  		  rxBlockReceived = 0;
  		  GPIO_PinInit(GPIO, BOARD_PORT, BOARD_PIN1, &pin1_config_output);
  	      GPIO_PinInit(GPIO, BOARD_PORT, BOARD_PIN2, &pin2_config_output);
  			p1_start_flag =1;
  			p1_on_flag=0;
  			p1_off_flag =0;
  			p1_end_delay_flag=0;
  			start_task = 1;
  			while(start_task)
  			{

  				if(p1_start_flag)
  				{
  				  systickCounter=0;
  				  p1_start_flag=0;
				  LED_OFF;
				  P1_LOW;
				  P2_HIGH;
  				  p1_on_flag =1;

  				}
  				if((systickCounter>=port.OnTime) & p1_on_flag)
  				{
  				  systickCounter=0;
  				  p1_on_flag = 0;
				  LED_OFF;
				  P1_HIGH;
				  P2_LOW;

  				  p1_off_flag = 1;

  				}
  				if((systickCounter>=port.OffTime) & p1_off_flag)
  				{
  				  port.Count--;
  				  if(0<port.Count){
  					p1_start_flag=1;
  				  }
  				  else{

  					 if(0!=port.Delay){
  						 p1_end_delay_flag=1;
  						 systickCounter =0;
  						  LED_OFF;
  						  P1_LOW;
  						  P2_HIGH;
  					 }
  					 else{
  						LED_ON;
  						P1_HIGH;
  						P2_LOW;
  					    GPIO_PinInit(GPIO, BOARD_PORT, BOARD_PIN1, &pin1_config_input);
  					    GPIO_PinInit(GPIO, BOARD_PORT, BOARD_PIN2, &pin2_config_output);
  						start_task=0;
  					 }
  				  }
  				  p1_off_flag = 0;

  				}
  				if((systickCounter>=port.Delay) & p1_end_delay_flag)
  				{
  					LED_ON;
  					P1_HIGH;
  					P2_LOW;
  				    GPIO_PinInit(GPIO, BOARD_PORT, BOARD_PIN1, &pin1_config_input);
  				    GPIO_PinInit(GPIO, BOARD_PORT, BOARD_PIN2, &pin2_config_output);
  					start_task =0;
  					p1_end_delay_flag = 0;

  				}

  			}

  	  }

    }
}


